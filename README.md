# TreuMoDa Mockups

Dieses Repository enthält die Mockups, die im Rahmen des BMBF-geförderten Projekts [TreuMoDa](https://www.treumoda.de) erstellt wurden. Das Projekt konzentriert sich auf die anonymisierte Weitergabe von Mobilitätsdaten über eine spezielle Treuhandstelle.

Im Projektverlauf wurden zwei Workshops mit Stakeholdern abgehalten.
In diesen Workshops testeten und bewerteten die Teilnehmer den Prozess der Treuhandstelle mithilfe der Mockups.
Diese Mockups sind als [Figma](https://www.figma.com/)-Exporte (.fig) in diesem Repository verfügbar.
Zusätzlich gibt es zu jeder Figma-Datei einen ZIP-Export, der alle Figma-Frames als statische Bilder im PDF- und PNG-Format enthält.

Das Repository enthält außerdem die Leitfäden für die Durchführung der Stakeholder-Interviews (.docx) und frühere Mockup-Entwürfe (.pdf), die mit [Balsamiq](https://balsamiq.com/) erstellt wurden.

Weitere Informationen zum Projekt TreuMoDa sind im [Abschlussbericht](https://publikationen.bibliothek.kit.edu/1000172026) zu finden.

Das Projekt wurde unter Förderkennzeichen 16DTM112A vom Bundesministerium für Forschung und Bildung (BMBF) gefördert und mit Mitteln der Europäischen Union unter NextGenerationEU finanziert.

## Installation

Die Mockups wurden mit dem Online-Tool Figma erstellt.
Es ist also keine Installation erforderlich, sondern die Erstellung eines Accounts auf der Homepage von [Figma](https://www.figma.com/).

## Benutzung

Nach der Erstellung eines Accounts auf Figma können die hier gespeicherten .fig-Dateien importiert und verwendet werden.

## Lizenz

Die Mockups wurden vom Karlsruher Institut für Techologie (KIT) innerhalb des TreuMoDa-Projektes erstellt und werden unter Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) zur Verfügung gestellt.

